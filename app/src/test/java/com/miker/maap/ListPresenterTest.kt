package com.miker.maap

import com.miker.maap.api.models.RijksMuseumApiResponse
import com.miker.maap.api.models.RijksMuseumArtObject
import com.miker.maap.api.models.RijksMuseumItemsCriteria
import com.miker.maap.ui.list.ListInterface
import com.miker.maap.ui.list.ListPresenter
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import rx.Observable
import rx.Scheduler
import rx.android.plugins.RxAndroidPlugins
import rx.android.plugins.RxAndroidSchedulersHook
import rx.schedulers.Schedulers

@RunWith(MockitoJUnitRunner::class)
class ListPresenterTest {
    private lateinit var view : ListInterface.View
    private lateinit var interactor : ListInterface.Interactor
    private lateinit var presenter : ListPresenter

    @Captor
    lateinit var captor: ArgumentCaptor<List<RijksMuseumArtObject>>

    @Before
    fun setup(){
        RxAndroidPlugins.getInstance().registerSchedulersHook(object : RxAndroidSchedulersHook() {
            override fun getMainThreadScheduler(): Scheduler {
                return Schedulers.immediate()
            }
        })

        view = Mockito.mock(ListInterface.View::class.java)
        interactor = Mockito.mock(ListInterface.Interactor::class.java)
        presenter = ListPresenter(interactor)
        presenter.attachView(view)
        MockitoAnnotations.openMocks(this)
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        RxAndroidPlugins.getInstance().reset()
    }

    @Test
    fun `When get items then items will be passed`() {
        Mockito.
            `when`(interactor.fetchItems(getTestCriteria()))
            .thenReturn(Observable.just(getTestResult()))

        presenter.getItems(getTestCriteria())

        verify(view, times(1)).showProgress()
        verify(view, times(1)).clearItems()
        verify(interactor, times(1)).fetchItems(getTestCriteria())
        verify(view, times(1)).hideProgress()
        verify(view).updateItems(MockitoHelper.capture(captor))
        Assert.assertEquals(getTestResult().artObjects, captor.value)
    }

    @Test
    fun `When get items raises an error a message is shown`() {
        Mockito.
        `when`(interactor.fetchItems(getTestCriteria()))
            .thenReturn(Observable.error(Throwable(message = "There is an exception to be handled")))

        presenter.getItems(getTestCriteria())

        verify(view, times(1)).showProgress()
        verify(view, times(1)).showError()
        verify(view, times(1)).hideProgress()
    }

    private fun getTestResult() : RijksMuseumApiResponse {
        val objects = mutableListOf<RijksMuseumArtObject>()
        objects.add(
            RijksMuseumArtObject(
            id = "test-id",
            title = "test title",
            webImage = null,
            headerImage= null,
            longTitle ="test title long",
            objectNumber = "123456",
            hasImage = false,
            principalOrFirstMaker = "test maker",
            links = null,
            productionPlaces = null)
        )

       return RijksMuseumApiResponse(
            count = 1,
            artObjects = objects
        )
    }

    private fun getTestCriteria(): RijksMuseumItemsCriteria   {
        return  RijksMuseumItemsCriteria(
            page = 0 ,
            pageSize = 1
        )
    }
}