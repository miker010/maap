package com.miker.maap

import com.miker.maap.api.models.RijksMuseumApiResponse
import com.miker.maap.api.models.RijksMuseumArtObject
import com.miker.maap.api.models.RijksMuseumItemsCriteria
import com.miker.maap.ui.details.DetailsInteractor
import com.miker.maap.ui.details.DetailsInterface
import com.miker.maap.ui.details.DetailsPresenter
import com.miker.maap.ui.list.ListInterface
import com.miker.maap.ui.list.ListPresenter
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import rx.Observable
import rx.Scheduler
import rx.android.plugins.RxAndroidPlugins
import rx.android.plugins.RxAndroidSchedulersHook
import rx.schedulers.Schedulers

@RunWith(MockitoJUnitRunner::class)
class DetailsPresenterTest {
    private lateinit var view : DetailsInterface.View
    private lateinit var interactor : DetailsInterface.Interactor
    private lateinit var presenter : DetailsPresenter

    @Before
    fun setup(){
        view = Mockito.mock(DetailsInterface.View::class.java)
        interactor = Mockito.mock(DetailsInterface.Interactor::class.java)
        presenter = DetailsPresenter(interactor)
        presenter.attachView(view)
        MockitoAnnotations.openMocks(this)
    }

    @Captor
    lateinit var captor: ArgumentCaptor<RijksMuseumArtObject>

    @Test
    fun `When an item is passed as argument it can be retrieved`() {
        presenter.getItem(getTestPassedArgument())
        verify(view, times(1)).updateItem(getTestPassedArgument())
        verify(view).updateItem(MockitoHelper.capture(captor))
        Assert.assertEquals(getTestPassedArgument(), captor.value)
    }

    private fun getTestPassedArgument() : RijksMuseumArtObject {
        return  RijksMuseumArtObject(
                id = "test-id",
                title = "test title",
                webImage = null,
                headerImage= null,
                longTitle ="test title long",
                objectNumber = "123456",
                hasImage = false,
                principalOrFirstMaker = "test maker",
                links = null,
                productionPlaces = null)
    }
}