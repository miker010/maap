package com.miker.maap.ui.list

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.miker.maap.R
import com.miker.maap.api.client.RijksMuseumApiClient
import com.miker.maap.api.models.RijksMuseumArtObject
import com.miker.maap.api.models.RijksMuseumItemsCriteria
import com.miker.maap.databinding.FragmentListBinding
import com.miker.maap.ui.base.EndlessRecyclerViewScrollListener
import com.miker.maap.ui.base.RecyclerItemClickListener
import com.miker.maap.ui.details.DetailsInteractor
import com.miker.maap.ui.details.DetailsInterface
import com.miker.maap.ui.details.DetailsPresenter

/**
 * For demo purposes the list fragment uses simple data binding and the
 * detail fragment uses data binding with a view model and live data,
 * providing even better options for automated testing purposes.
 */
class ListFragment : Fragment(), ListInterface.View {
    private val PAGE_SIZE = 10

    private val fetchedItems = mutableListOf<RijksMuseumArtObject>()
    private var binding: FragmentListBinding? = null
    private lateinit var presenter: ListInterface.Presenter
    private lateinit var interactor : ListInterface.Interactor
    private var scrollListener: EndlessRecyclerViewScrollListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        FragmentListBinding.inflate(inflater, container, false).let { binding ->
            this.binding = binding
            interactor = ListInteractor()
            presenter = ListPresenter(interactor)
            presenter.attachView(this)
            return binding.root
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        presenter.getItems(RijksMuseumItemsCriteria(page = 0, pageSize = PAGE_SIZE))
    }

    override fun showProgress() {
        activity?.runOnUiThread{
            binding?.listProgressBar?.visibility = View.VISIBLE
        }
    }

    override fun hideProgress() {
        activity?.runOnUiThread{
            binding?.listProgressBar?.visibility = View.GONE
        }
    }

    override fun clearItems() {
        val recyclerView = view?.findViewById(R.id.list_recycler_view) as RecyclerView
        (recyclerView.adapter as? ListItemAdapter)?.clear()
    }

    override fun updateItems(items: List<RijksMuseumArtObject>) {
        fetchedItems.addAll(items)

        val recyclerView = view?.findViewById(R.id.list_recycler_view) as RecyclerView
        (recyclerView.adapter as? ListItemAdapter)?.let { adapter ->
            adapter.feed(items)
            adapter.notifyDataSetChanged()
        }
    }

    override fun showError() {
        activity?.runOnUiThread {
            Toast.makeText(activity, R.string.list_cannot_fetch, Toast.LENGTH_SHORT).show()
        }
    }

    private fun init() {
        val linearLayoutManager = LinearLayoutManager(this.activity)
        val adapter = ListItemAdapter(context= this.activity as Context)
        adapter.feed(fetchedItems)

        binding?.let { binding ->
            binding.listRecyclerView.let {recyclerView ->
                recyclerView.adapter = adapter
                recyclerView.layoutManager = linearLayoutManager

                recyclerView.addOnItemTouchListener(
                    RecyclerItemClickListener(context, object :
                        RecyclerItemClickListener.OnItemClickListener {
                        override fun onItemClick(view: View?, position: Int) {
                            val item = adapter.items.get(position)
                            val artObject = (item as DataItem.ArtObjectItem).artObject
                            Log.d("ListFragment", "Click on ${artObject.title}")
                            val action = ListFragmentDirections.actionListFragmentToDetailsFragment(artObject)
                            findNavController().navigate(action)
                        }
                    })
                )

                scrollListener = object : EndlessRecyclerViewScrollListener(linearLayoutManager) {
                    override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                        presenter.getItems(RijksMuseumItemsCriteria(page = page, pageSize = PAGE_SIZE))
                    }
                }

                scrollListener?.let { listener ->
                    recyclerView.addOnScrollListener(listener)
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}