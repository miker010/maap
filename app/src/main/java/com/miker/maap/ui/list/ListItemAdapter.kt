package com.miker.maap.ui.list

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.miker.maap.R
import com.miker.maap.api.models.RijksMuseumArtObject
import com.miker.maap.databinding.ListItemCellBinding
import com.miker.maap.databinding.ListItemHeaderCellBinding
import com.squareup.picasso.Picasso
import java.lang.Exception

class ListItemAdapter (private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val IMAGE_WIDTH = 100
    private val IMAGE_SIZE = 60
    private val ITEM_VIEW_TYPE_HEADER = -1
    private val ITEM_VIEW_TYPE_ITEM = -2

    var items = mutableListOf<DataItem>()

    fun clear(){
        items.clear()
    }

    fun feed (feedItems : List<RijksMuseumArtObject>?) {
        var newItems = when (feedItems){
            null -> mutableListOf(DataItem.HeaderItem(context.getString(R.string.list_progress_fetch)))
            else -> {
                val groupedList = feedItems.groupBy { it.principalOrFirstMaker }
                var preparedList = ArrayList<DataItem>()

                for(key in groupedList.keys){
                    val header = DataItem.HeaderItem(key)
                    if (items.find { it.id == header.id } == null) {
                        preparedList.add(header)
                    }

                    for(value in groupedList.getValue(key)){
                        preparedList.add(DataItem.ArtObjectItem(value))
                    }
                }
                preparedList
            }
        }
        items.addAll(newItems)
    }

    inner class ArtObjectViewHolder (val binding: ListItemCellBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(artObject : DataItem.ArtObjectItem) {
            with(binding){
                listItemTitleShort.text = artObject.artObject.title
                listItemTitleLong.text = artObject.artObject.longTitle

                artObject.artObject.webImage?.url?.let { uri ->
                    Picasso.get()
                        .load(uri)
                        .centerCrop()
                        .resize(IMAGE_WIDTH, IMAGE_SIZE)
                        .error(R.mipmap.ic_launcher)
                        .placeholder(R.drawable.progress)
                        .into(listItemImage)
                } ?: Picasso.get().load(R.mipmap.ic_launcher)
            }
        }
    }

    inner class HeaderViewHolder (val binding: ListItemHeaderCellBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(header: DataItem.HeaderItem) {
            binding.listItemHeaderTitle.text = header.header
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when ( items.get(position)) {
            is DataItem.HeaderItem -> ITEM_VIEW_TYPE_HEADER
            is DataItem.ArtObjectItem -> ITEM_VIEW_TYPE_ITEM
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)

        return when (viewType){
            ITEM_VIEW_TYPE_HEADER -> {
                val binding = ListItemHeaderCellBinding.inflate(inflater)
                HeaderViewHolder(binding)
            }
            ITEM_VIEW_TYPE_ITEM -> {
                val binding = ListItemCellBinding.inflate(inflater)
                ArtObjectViewHolder(binding)
            }
            else -> throw Exception("Unexpected view type $viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder) {
            is ArtObjectViewHolder -> {
                ((items.get(position) as DataItem.ArtObjectItem)).let { artObject ->
                    holder.bind(artObject)
                }
            }
            is HeaderViewHolder -> {
                (items.get(position) as DataItem.HeaderItem).let { headerItem ->
                    holder.bind(headerItem)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }
}