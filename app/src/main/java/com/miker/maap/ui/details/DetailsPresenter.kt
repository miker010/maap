package com.miker.maap.ui.details

import com.miker.maap.api.models.RijksMuseumArtObject

class DetailsPresenter (private val interactor: DetailsInterface.Interactor) : DetailsInterface.Presenter{
    private var view: DetailsInterface.View? = null

    override fun attachView(view: DetailsInterface.View) {
        if (this.view == null) {
            this.view = view
        }
    }

    override fun getItem(artObject: RijksMuseumArtObject) {
        view?.updateItem(artObject)
    }
}