package com.miker.maap.ui.list

import com.miker.maap.api.models.RijksMuseumApiResponse
import com.miker.maap.api.models.RijksMuseumArtObject
import com.miker.maap.api.models.RijksMuseumItemsCriteria
import rx.Observable

class ListInterface {
    interface View{
        fun showProgress()
        fun hideProgress()
        fun clearItems()
        fun updateItems(items: List<RijksMuseumArtObject>)
        fun showError()
    }

    interface Presenter {
        fun attachView(view: ListInterface.View)
        fun getItems(criteria: RijksMuseumItemsCriteria)
    }

    interface Interactor {
        fun fetchItems(criteria : RijksMuseumItemsCriteria) : Observable<RijksMuseumApiResponse>
    }
}