package com.miker.maap.ui.details

import com.miker.maap.api.models.RijksMuseumArtObject

class DetailsInterface {
    interface View{
        fun updateItem(artObject: RijksMuseumArtObject)
    }

    interface Presenter {
        fun attachView(view: View)
        fun getItem(artObject: RijksMuseumArtObject)
    }

    interface Interactor {
    }
}