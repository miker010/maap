package com.miker.maap.ui.list

import com.miker.maap.api.models.RijksMuseumArtObject

sealed class DataItem {
    abstract val id: String

    data class ArtObjectItem(val artObject: RijksMuseumArtObject): DataItem() {
        override val id = artObject.id
    }

    data class HeaderItem(val header: String): DataItem() {
        override val id = header.hashCode().toString()
    }
}