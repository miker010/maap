package com.miker.maap.ui.list

import com.miker.maap.api.client.RijksMuseumApiClient
import com.miker.maap.api.models.RijksMuseumApiResponse
import com.miker.maap.api.models.RijksMuseumItemsCriteria
import rx.Observable

open class ListInteractor : ListInterface.Interactor {

    private val client = RijksMuseumApiClient()

    override fun fetchItems(criteria: RijksMuseumItemsCriteria) : Observable<RijksMuseumApiResponse>{
        return client.getItems(criteria)
    }
}