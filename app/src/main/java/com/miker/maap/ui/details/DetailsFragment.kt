package com.miker.maap.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import com.miker.maap.R
import com.miker.maap.api.models.RijksMuseumArtObject
import com.miker.maap.databinding.FragmentDetailsBinding
import com.squareup.picasso.Picasso

/**
 * For demo purposes the list fragment uses simple data binding and the
 * detail fragment uses data binding with a view model and live data,
 * providing even better options for automated testing purposes.
 */
class DetailsFragment : Fragment(), DetailsInterface.View {

    companion object {
        private const val IMAGE_WIDTH = 280
        private const val IMAGE_HEIGHT = 280

        @BindingAdapter("bind:imageUrl")
        @JvmStatic
        fun bindImageUrl(view: ImageView, path: String?) {
            if (path==null) {
                Picasso.get().load(R.mipmap.ic_launcher)
            }
            else {
                Picasso.get()
                    .load(path)
                    .centerInside()
                    .resize(IMAGE_WIDTH, IMAGE_HEIGHT)
                    .error(R.mipmap.ic_launcher)
                    .placeholder(R.drawable.progress)
                    .into(view)
            }
        }
    }

    private lateinit var binding: FragmentDetailsBinding
    private lateinit var presenter: DetailsInterface.Presenter
    private lateinit var interactor : DetailsInterface.Interactor

    private var viewModel = DetailsViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?): View {
        binding = FragmentDetailsBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        interactor = DetailsInteractor()
        presenter = DetailsPresenter(interactor)
        presenter.attachView(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bundle = arguments
        if (bundle != null){
            val args = DetailsFragmentArgs.fromBundle(bundle)
            presenter.getItem( args.artObject)
        }
    }

    override fun updateItem(artObject: RijksMuseumArtObject) {
        with(viewModel){
            title.value = artObject.title
            titleLong.value = artObject.longTitle
            objectNo.value = artObject.objectNumber
            imageUri.value = artObject.webImage?.url
        }
    }
}