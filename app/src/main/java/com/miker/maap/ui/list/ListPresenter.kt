package com.miker.maap.ui.list

import com.miker.maap.api.models.RijksMuseumItemsCriteria

open class ListPresenter (private val interactor: ListInterface.Interactor) : ListInterface.Presenter {
    private var view: ListInterface.View? = null

    override fun attachView(view: ListInterface.View) {
        if (this.view == null) {
            this.view = view
        }
    }

    override fun getItems(criteria: RijksMuseumItemsCriteria) {
        view?.let {view ->
            if (criteria.page == 0){
                view.clearItems()
            }

            view.showProgress()
            interactor.fetchItems(criteria).subscribe( { event ->
                view.hideProgress()
                view.updateItems(event.artObjects)
            },
            {
                view.hideProgress()
                view.showError()
            })
        }
    }
}