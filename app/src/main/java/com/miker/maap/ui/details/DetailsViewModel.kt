package com.miker.maap.ui.details

import androidx.lifecycle.MutableLiveData

class DetailsViewModel {
    var title: MutableLiveData<String> = MutableLiveData()
    var objectNo : MutableLiveData<String> = MutableLiveData()
    var titleLong : MutableLiveData<String> = MutableLiveData()
    var firstMaker : MutableLiveData<String> = MutableLiveData()
    var imageUri : MutableLiveData<String> = MutableLiveData()
}