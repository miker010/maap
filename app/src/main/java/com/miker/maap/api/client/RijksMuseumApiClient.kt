package com.miker.maap.api.client

import com.miker.maap.AppConstants
import com.miker.maap.api.models.RijksMuseumApiResponse
import com.miker.maap.api.models.RijksMuseumItemsCriteria
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class RijksMuseumApiClient ()  {
    val DEFAULT_SORT_BY = "artist"

    val apiInterface : RijksMuseumApiInterface
        get() { return  retrofit.create(RijksMuseumApiInterface::class.java) }

    val retrofit : Retrofit

    init {
        val client = OkHttpClient().newBuilder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = if (com.miker.maap.BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else
                    HttpLoggingInterceptor.Level.NONE
            })
            .connectTimeout(AppConstants.RIJKSMUSEUM_API_TIME_OUT_SEC, TimeUnit.SECONDS)
            .readTimeout(AppConstants.RIJKSMUSEUM_API_TIME_OUT_SEC, TimeUnit.SECONDS)
            .writeTimeout(AppConstants.RIJKSMUSEUM_API_TIME_OUT_SEC, TimeUnit.SECONDS)
            .build()

        retrofit = Retrofit.Builder()
            .baseUrl(AppConstants.RIJKSMUSEUM_BASE_URI)
            .client(client)
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
    }

    fun getItems(criteria: RijksMuseumItemsCriteria) : Observable<RijksMuseumApiResponse> {
        return apiInterface.getItems(key = AppConstants.RIJKSMUSEUM_API_KEY ,
                                     page= criteria.page,
                                     pageSize = criteria.pageSize,
                                     sortBy = DEFAULT_SORT_BY
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}
