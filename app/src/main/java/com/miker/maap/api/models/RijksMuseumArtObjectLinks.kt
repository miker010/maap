package com.miker.maap.api.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class RijksMuseumArtObjectLinks (val self: String, val web: String) : Parcelable