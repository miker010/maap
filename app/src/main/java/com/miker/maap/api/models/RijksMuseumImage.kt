package com.miker.maap.api.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class RijksMuseumImage (val guid: String, val url: String, val width: Int, val height: Int) :
    Parcelable