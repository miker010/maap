package com.miker.maap.api.models

data class RijksMuseumItemsCriteria (var page: Int = 0, var pageSize: Int = 10)