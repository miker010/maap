package com.miker.maap.api.client

import com.miker.maap.AppConstants
import com.miker.maap.api.models.RijksMuseumApiResponse
import retrofit2.http.*
import rx.Observable

interface RijksMuseumApiInterface {
    @GET(AppConstants.RIJKSMUSEUM_GET_ITEMS_PATH)
    fun getItems(@Query("key") key:String,
                 @Query("p") page: Int,
                 @Query("ps") pageSize: Int,
                 @Query("s") sortBy: String): Observable<RijksMuseumApiResponse>
}