package com.miker.maap.api.models

data class RijksMuseumApiResponse (val artObjects : List<RijksMuseumArtObject>, val count: Int)