package com.miker.maap.api.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class RijksMuseumArtObject (val id: String, val title: String,
                                 val webImage: RijksMuseumImage? = null,
                                 val headerImage: RijksMuseumImage? = null,
                                 val longTitle:String,
                                 val objectNumber: String,
                                 val hasImage: Boolean,
                                 val principalOrFirstMaker : String,
                                 val links: RijksMuseumArtObjectLinks? = null,
                                 val productionPlaces : List<String>? = null) : Parcelable