package com.miker.maap

class AppConstants {
    companion object{
        const val RIJKSMUSEUM_BASE_URI = "https://www.rijksmuseum.nl/api/nl/"
        const val RIJKSMUSEUM_GET_ITEMS_PATH = "collection/"
        const val RIJKSMUSEUM_API_KEY = "0fiuZFh4"
        const val RIJKSMUSEUM_API_TIME_OUT_SEC = 30L
    }
}