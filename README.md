# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Mikes Android Assessment Programming (MAAP) evaluation
* Version 1

### This app demonstrates the following: 
* Kotlin 
* Coding style
* Fetching JSON from an API using Retrofit and interception (for logging purposes)
* Observing data (RX)
* Subscribing
* MVP
* Databinding (both simple data binding and databind using a view model)
* MVVM
* ViewModel
* RecyclerView, with sections and 'infinite' scrolling
* Navigation
* Parcelable items
* Unit testing
* Mockito
* Mocking data
* Error handling

### How do I get set up? ###

* Use Android Studio 4.2.1 or above
* The external dependencies are listed in the apps build.gradle-file
* Gradle will take of all external dependencies
* Other dependencies (added through code): EndLessRecyclerViewScrollListener and RecyclerItemClickListener

### Who do I talk to? ###

* You know how to find me

